//dependencies
const express = require('express')
const mongoose = require('mongoose')
//server setup
const app = express()
const port = 3001
//This allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')


//database connection
mongoose.connect("mongodb+srv://admin:Password30x__@cluster0.s5jd2.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database."))
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Routes(Base URI for task route)
//Allows all the task routes created in the taskRoute.js file to use '/tasks' route

app.use("/tasks", taskRoute)
//http://localhost:3001/tasks


app.listen(port, () => console.log(`Server is running at post ${port}`))