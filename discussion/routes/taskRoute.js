const express = require('express')
const taskController = require('../controllers/taskController.js')
//Create a router instance that functions as a middleware and routing system
//allows access to HTTP method middleswares that makes it easier to create routes for our application
const router = express.Router()

//Route to get all the tasks
//http://localhost:3001/tasks/
router.get("/", (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//post route
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(result => res.send(result))
})

//delete route
//http://localhost:3001/tasks/:id
//The colon (:) is an identifier that helps create a dynamic route which allos us to supply information in that URL
// The word that comes after the colon symbol will be the name of the URL parameter
// ":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (req, res) => {
    //URL parameter values are accessed via the request object's "params" property
    //The property name of this object will match the given URL parameter name
    //In this case "id" is the name of the parameter
    //
    taskController.deleteTask(req.params.id).then(result => res.send(result))
})

//Update a task
router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})

//Get specific task
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//Update a task to complete
router.put("/:id/complete", (req, res) => {
    taskController.updateTaskComplete(req.params.id, req.body).then(result => res.send(result))
})

module.exports = router;